extends KinematicBody2D

const UP = Vector2(0, -1)
var motion = Vector2()
var isAlive = true
var isDeadTimeout = 100
var isGameOver = false

export var speed = 200
export var gravity = 15
export var jumpForce = -500

func _physics_process(delta):
	motion.y += gravity

	if !isAlive and isDeadTimeout <= 0:
		if !isGameOver:
			_game_over()
			isGameOver = true
		return

	if !isAlive:
		# Não entendi essa parte direito HELP RONEI
		isDeadTimeout -= (delta * 100)
		
		motion.x = -(speed - 50)
		motion = move_and_slide(motion, UP)
		return

	var isOnFloor = is_on_floor()
	var isGoingRight = Input.is_action_pressed('ui_right')
	var isGoingLeft = Input.is_action_pressed('ui_left')
	var isJumping = Input.is_action_just_pressed('ui_up')

	if isOnFloor:
		if isGoingRight:
			$AnimatedSprite.play("walk")

		if isGoingLeft:
			$AnimatedSprite.play("walk")
		
		if isJumping:
			motion.y = jumpForce
			$AnimatedSprite.play("jump")

	if isGoingRight:
		$AnimatedSprite.flip_h = false
		motion.x = speed
		
	elif isGoingLeft:
		$AnimatedSprite.flip_h = true
		motion.x = -speed

	else:
		motion.x = 0
		if isOnFloor and !isJumping:
			$AnimatedSprite.play("walk")
			$AnimatedSprite.stop()
	
	motion = move_and_slide(motion, UP)

func _on_Touch2Die2_area_entered(_area):
	isAlive = false
	$AnimatedSprite.play("die")

func _game_over():
	print("== BUSTED ==")
	
